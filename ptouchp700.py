#!/usr/bin/env python3

# Brother PT-P700 Label printer control utility - Python port of the original C code for Linux
# https://github.com/abelits/ptouch-770

# /*
#   ptouch-770-write.c
#   Brother PT-H500/P700/E500 printer control utility.
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 2 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
# */

import usb.core #pyusb
import binascii
import struct
import time

def main():
  import sys
  argv = sys.argv
  argc = len(argv)

  if (argc < 2):
    print(f"Usage: {argv[0]} <image.file>", file=sys.stderr)
    return 1

  from PIL import Image
  im = Image.open(argv[1])
  printer = PtouchP700()
  printer.print_raster(printer.raster_from_image(im))
  printer.dev.reset()

class PtouchP700(object):
  BULK_IN = 0x81
  BULK_OUT = 0x02
  COL_HEIGHT=16
  #/* Media minimum and maximum width in mm */
  MEDIA_WIDTH_MIN = 4
  MEDIA_WIDTH_MAX = 24

  def __init__(self, cut_after=False):
    self.cut_after = cut_after

    #/* Find Brother PT-H500/P700/E500 device */
    vid = int("04f9", 16)
    pid = int("205e", 16) #PT-H500
    pid = int("205f", 16) #PT-E500
    pid = int("2061", 16) #PT-P700

    dev = usb.core.find(idVendor=vid, idProduct=pid)
    dev.set_configuration()
    self.dev = dev

  def raster_from_image(self, img, invert=False):
    if img.height != 128:
      print("Invalid height, expected 128 px, got {} px.".format(img.height))
      return None
    rd = [0] * (img.width * self.COL_HEIGHT)
    for x in range(img.width):
      for y in range(128):
        p = img.getpixel((x, y))
        if invert ^ (not p):
          rd[x*self.COL_HEIGHT + y//8] |= (1 << (7-(y%8)))
    return rd

  def print_raster(self, raster_data):
    st = time.monotonic()
    dev = self.dev
    #/* Initialization */
    if 102 != dev.write(self.BULK_OUT, [0] * 100 + [0x1b, 0x40]):
      return 1

    #/* Get status */
    if 3 != dev.write(self.BULK_OUT, [0x1b, 0x69, 0x53]):
      return 1
    media_width = dev.read(self.BULK_IN, 32, 1000).tolist()[10]
    if not (self.MEDIA_WIDTH_MIN <= media_width <= self.MEDIA_WIDTH_MAX):
      print("Replace label tape cartridge; unexpected media_width: {}.".format(media_width), file=sys.stderr)
      return 1

    l = len(raster_data)
    if 0 != (l % self.COL_HEIGHT):
      print("Invalid raster_data: {} not multiple of {} bytes.".format(l, self.COL_HEIGHT), file=sys.stderr)
      return 1

    #/* Dynamic command mode setting: raster */
    if 4 != dev.write(self.BULK_OUT,[0x1b, 0x69, 0x61, 0x01]):
      return 1

    # /*
    #   Print information (parameters) command:
    #   1: flags: printer recovery on, set media width,
    #   2: media type: unused
    #   3: media width: value from the printer
    #   4: media length: unused
    #   5-8: number of raster columns, lsb first
    #   9: starting page
    #   10: unused
    # */
    cmd_buffer = [
        0x1B, 0x69, 0x7A, 0x84, #1
        0, media_width, 0, #2 3 4
    ] + list(struct.pack('<I', l//self.COL_HEIGHT)) + [ #5 6 7 8
        0, 0, #9 10
        0x1B, 0x69, 0x4D, 0x40, #auto cut before output
        0, 0, 0, 0,
        0x1B, 0x69, 0x4B, (0x08 if self.cut_after else 0x00), #cut after output
        0x1b, 0x69, 0x64, 0x0e, 0x00, #margins 2 mm
        0x4D, 0x00 #compression: none
    ]

    if 32 != dev.write(self.BULK_OUT, cmd_buffer):
        return 1

    i = 0
    while (i < l):
      dev.write(self.BULK_OUT, [0x47, 16, 0] + raster_data[i:i+16])
      dev.read(self.BULK_IN, 32, 1000)
      i += 16

    #/* Print */
    if 1 != dev.write(self.BULK_OUT, [0x1a]):
      return 1

    #Estimate print time for the print speed of 30 mm/s and resolution of 180 dpi.
    ptime = ((l/self.COL_HEIGHT) / (180/25.4)) / 30.0
    et = time.monotonic() + 5.000 + ptime
    while time.monotonic() < et:
      time.sleep(0.100)
      resp = dev.read(self.BULK_IN, 32, 1000)
      if len(resp) == 32 and resp[18] == 6 and resp[19] == 0:
        #phase change to editing state
        break

    print("printed in {} seconds".format(time.monotonic()-st))
    return 0


if __name__ == "__main__":
    main()
